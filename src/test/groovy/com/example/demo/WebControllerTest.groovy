package com.example.demo

import com.fasterxml.jackson.databind.ObjectMapper
import org.spockframework.spring.SpringBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import org.springframework.http.HttpStatus

import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

// https://blog.allegro.tech/2018/04/Spring-WebMvcTest-with-Spock.html
// https://github.com/rafal-glowinski/mvctest-spock/blob/master/src/test/groovy/com/rg/webmvctest/rest/UserRegistrationValidationSpec.groovy
// https://www.baeldung.com/integration-testing-in-spring
// https://www.baeldung.com/spock-stub-mock-spy

@AutoConfigureMockMvc
@WebMvcTest
class WebControllerTest extends Specification {

    @Autowired
    private MockMvc mockMvc

    @Autowired
    ObjectMapper objectMapper

    @SpringBean
    private PostService postService = Stub()

    @SpringBean
    private PostRepository postRepository = Stub()

    private Post getNewPost(Integer id, String title, String description) {
        def post = new Post()
        post.id = id
        post.title = title
        post.description = description

        return post
    }

    private List<Post> getNewPosts() {
        def test1 = getNewPost(1, "test1", "This is test1")
        def test2 = getNewPost(2, "test2", "This is test2")

        List<Post> posts = new ArrayList<Post>()

        posts.add(test1)
        posts.add(test2)

        return posts
    }

    def "when GET /greeting then status 200 and 'Hello world!'"() {
        expect:
            this.mockMvc.perform(get("/greeting"))
                .andExpect(status().isOk())
                .andReturn()
                .response
                .contentAsString == "Hello world!"
    }

    def "when GET /all then status 200 and a list of posts in Json"() {
        given:
        postService.getAllPosts() >> getNewPosts()

        when:
        def results = this.mockMvc.perform(get("/all"))

        then:
            results.andExpect(status().isOk())
            results.andExpect(jsonPath('$[0].title').value("test1"))
            results.andExpect(jsonPath('$[0].description').value("This is test1"))
    }

    def "when GET /get/{id} then status 200 and a post in Json"() {
        given:
        postService.getOnePost(_ as Integer) >> getNewPost(1, "test1", "This is test1")

        when:
        def results = this.mockMvc.perform(get("/get/{id}", "1"))

        then:
        results.andExpect(status().isOk())
        results.andExpect(jsonPath('$.title').value("test1"))
        results.andExpect(jsonPath('$.description').value("This is test1"))
    }

    def "when POST /add then status 200 and a post in Json that just added"() {
        given:
        def request = [
                id        : 1,
                title     : "test1",
                description : "This is test1"
        ]

        postService.addNewPost(_ as Post) >> getNewPost(1, "test1", "This is test1")

        when:
        def results = this.mockMvc.perform(post("/add")
                .contentType(APPLICATION_JSON).content(objectMapper.writeValueAsString(request))).andReturn().response

        then:
        results.status == HttpStatus.OK.value()

        and:
        with (objectMapper.readValue(results.contentAsString, Map)) {
            it.title == 'test1'
            it.description == 'This is test1'
        }
    }

    def "when PUT /update/{id} then status 200 and a post in Json that just updated"() {
        given:
        def request = [
                id        : 1,
                title     : "test1",
                description : "That is test1"
        ]

        postService.updatePost(_ as Post, _ as Integer) >> getNewPost(1, "test1", "That is test1")

        when:
        def results = this.mockMvc.perform(put("/update/{id}", "1")
                .contentType(APPLICATION_JSON).content(objectMapper.writeValueAsString(request))).andReturn().response

        then:
        results.status == HttpStatus.OK.value()

        and:
        with (objectMapper.readValue(results.contentAsString, Map)) {
            it.title == "test1"
            it.description == "That is test1"
        }
    }

    def "when DELETE /delete/{id} then status 200 and void"() {
        given:
        postService.deletePost(4) >> void

        when:
        def results = this.mockMvc.perform(get("/get/{id}", "4"))

        then:
        results.andExpect(status().isOk())
    }

}