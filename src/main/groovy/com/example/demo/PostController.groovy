package com.example.demo

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
class PostController {

    @Autowired
    private PostService postService

    @GetMapping("/greeting")
    @ResponseBody String greeting() {
        return "Hello world!"
    }

    @GetMapping(path="/all")
    @ResponseBody Iterable<Post> getAllPosts() {
        return postService.getAllPosts()
    }

    @GetMapping("/get/{id}")
    Post getOnePost(@PathVariable Integer id) {
        return postService.getOnePost(id)
    }

    @PostMapping(path="/add")
    Post addNewPost (@RequestBody Post post) {
        return postService.addNewPost(post)
    }

    @PutMapping("/update/{id}")
    Post updatePost(@RequestBody Post newPost, @PathVariable Integer id) {
        return postService.updatePost(newPost, id)
    }

    @DeleteMapping("/delete/{id}")
    void deletePost(@PathVariable Integer id) {
        postService.deletePost(id)
    }
}
