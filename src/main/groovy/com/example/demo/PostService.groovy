package com.example.demo

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class PostService {

    @Autowired
    private PostRepository postRepository

    Iterable<Post> getAllPosts() {
        return postRepository.findAll()
    }

    Post getOnePost(Integer id) {
        return postRepository.findById(id)
                .orElseThrow(() -> new PostNotFoundException(id))
    }

    Post addNewPost (Post post) {
        return postRepository.save(post)
    }

    Post updatePost(Post newPost, Integer id) {
        return postRepository.findById(id)
                .map(post -> {
                    post.setTitle(newPost.getTitle())
                    post.setDescription(newPost.getDescription())
                    return postRepository.save(post)
                })
                .orElseGet(() -> {
                    newPost.setId(id)
                    return postRepository.save(newPost)
                })
    }

    void deletePost(Integer id) {
        postRepository.deleteById(id)
    }
}
