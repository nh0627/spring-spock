package com.example.demo

 class PostNotFoundException extends RuntimeException {

    PostNotFoundException(Integer id) {
        super("Could not find employee " + id)
    }
}
