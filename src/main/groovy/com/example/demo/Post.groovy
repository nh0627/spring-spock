package com.example.demo

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
 class Post {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id
    private String title
    private String description

    Post() {
    }

    Post(Integer id, String title, String description) {
        this.id = id
        this.title = title
        this.description = description
    }

    Integer getId() {
        return id
    }

     void setId(Integer id) {
        this.id = id
    }

     String getTitle() {
        return title
    }

     void setTitle(String title) {
        this.title = title
    }

     String getDescription() {
        return description
    }

     void setDescription(String description) {
        this.description = description
    }

}
